"use strict";

document.addEventListener("DOMContentLoaded", () => {
  const images = document.querySelectorAll(".image-to-show");
  const stopBtn = document.querySelector(".stop-btn");
  const resumeBtn = document.querySelector(".resume-btn");

  let currentIndex = 0;
  let intervalId = null;

  function showImage(index) {
    images.forEach((image, i) => {
      if (i === index) {
        image.style.display = "block";
      } else {
        image.style.display = "none";
      }
    });
  }

  function startSlideshow() {
    showImage(currentIndex);
    currentIndex = (currentIndex + 1) % images.length;
  }

  function start() {
    intervalId = setInterval(startSlideshow, 3000);
  }

  function stop() {
    clearInterval(intervalId);
  }

  stopBtn.addEventListener("click", () => {
    stop();
  });

  resumeBtn.addEventListener("click", () => {
    start();
  });

  start();
});


//   Теоретичні питання
// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

1. setTimeout() використовується для запуску вказаної функції після певної затримки вказаної у мілісекундах
  setInterval() використовується для запуску вказаної функції періодично з певним інтервалом часу
2. Якщо передати нульову затримку у функцію setTimeout(), це не призведе до миттєвого запуску функції, якщнавіть функція викується 0 секунд ва всерівнбуде чекати свї черги викнання
